import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.UpdateValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by xuyujie on 2017/5/24.
 */
public class TestProcess {

    private ArrayList<Testparam> tps  = new ArrayList<Testparam>() ;
    private Properties prop = new Properties();

    public void parse (String[] msgs) {
        try {
            for (String msg : msgs) {
                String[] values = msg.split(",", -1);
                float click = Float.valueOf(values[3]);
                float click_count = Float.valueOf(values[4]);
                float view = Float.valueOf(values[5]);
                float view_count = Float.valueOf(values[6]);
                int fold = Integer.valueOf(values[7]);
                tps.add(new Testparam(values[0], values[1],values[2], click,click_count,view, view_count,fold));
            }
        SheetCredential sc = new SheetCredential();
        Sheets service = sc.getSheetsService();
        HashMap<String,ArrayList<Testparam>> mapper = new HashMap<String,ArrayList<Testparam>>();
        for (Testparam tp: tps) {
            ArrayList<Testparam> temp = new ArrayList<Testparam>();
            if (mapper.get(tp.getModeltype()+"_" + tp.getAdshost()) == null) {
                temp.add(tp);
                mapper.put(tp.getModeltype() + "_" + tp.getAdshost(), temp);
            } else {
                temp = mapper.get(tp.getModeltype() + "_" + tp.getAdshost());
                temp.add(tp);
                Collections.sort(temp);
                mapper.put(tp.getModeltype() + "_" + tp.getAdshost(), temp);
            }
        }

        for (String key : mapper.keySet()) {
            String spreadsheetId = getSheetid(key);
            if (spreadsheetId != null) {
                List<List<Object>> content = new ArrayList<List<Object>>();
                ValueRange oRange = new ValueRange();
                ArrayList<Object> row = new ArrayList<Object>();
                ArrayList<Testparam> temp = mapper.get(key);
                int index = 0;
                for (Testparam testparam : mapper.get(key)) {
                    if (row.size() == 0)
                        row.add(0 ,testparam.getDate());
                    row.add(++ index ,testparam.getClick());
                    row.add(++ index,testparam.getView());
                    row.add(++ index,testparam.getCtr(1));
                    row.add(++ index,testparam.getClick_count());
                    row.add(++ index,testparam.getView_count());
                    row.add(++ index,testparam.getCtr(2));

                }
                content.add(row);
                oRange.setValues(content);
                int oldcontent = service.spreadsheets().values()
                        .get(spreadsheetId, "A:B")
                        .execute().getValues().size();
                int size = Integer.valueOf(oldcontent) + 1 ;
                String rangestr = String.format("A%s:M%s",String.valueOf(index),String.valueOf(size));
                UpdateValuesResponse oResp1 = service.spreadsheets().values().update(spreadsheetId,rangestr,oRange).setValueInputOption("USER_ENTERED").execute();

            }

        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getSheetid(String sheetid) {
        try {
            prop.load(this.getClass().getClassLoader().getResourceAsStream("googleApiconf"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop.getProperty(sheetid);
    }
    public ArrayList<Testparam> getTestparams() {
        return this.tps;
    }

    public ArrayList<Object> process(Testparam tp) {
        ArrayList<Object> record = new ArrayList<Object>();
        record.add(0,tp.getDate().toString());
        return record;
     //   record.add(1,)
    }

    public static void main(String args[]) {
        TestProcess tpc = new TestProcess();
        String[] s = new String[2];
        s[0] = "loan,958,0117,5.1,3.2,4.3,2.2,2";
        s[1] = "loan,958,0117,5,3,4,2,1";


        tpc.parse(s);
    }

}
