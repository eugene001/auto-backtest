import java.util.Date;

/**
 * Created by xuyujie on 2017/5/24.
 */
public class Testparam implements Comparable<Testparam> {

    private String modeltype;
    private String date;
    private String adshost;
    private float click;
    private float click_count;
    private float view;
    private float view_count;
    private int fold;

    public float getCtr (int type) {
        switch (type) {
            case 1: {
                return click/view;
            }
            case 2: {
                return click_count/view_count;
            }
            default:
                return 0;
        }

    }

    public Testparam(String modeltype, String adshost, String date, float click,float click_count, float view, float view_count,int fold) {
        this.modeltype = modeltype;
        this.date = date;
        this.adshost = adshost;
        this.click = click;
        this.click_count = click_count;
        this.view = view;
        this.view_count = view_count;
        this.fold = fold;
    }

    public String getModeltype() {
        return modeltype;
    }

    public void setModeltype(String modeltype) {
        this.modeltype = modeltype;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAdshost() {
        return adshost;
    }

    public void setAdshost(String adshost) {
        this.adshost = adshost;
    }

    public float getClick() {
        return click;
    }

    public float getClick_count() {
        return click_count;
    }

    public float getView() {
        return view;
    }

    public void setView(float view) {
        this.view = view;
    }

    public float getView_count() {
        return view_count;
    }

    public void setView_count(float view_count) {
        this.view_count = view_count;
    }

    public int getFold() {
        return fold;
    }

    @Override
    public int compareTo(Testparam o) {
        if (o.getFold() < fold)
            return 1;
        else if (o.getFold() == fold)
            return 0;
        else
            return -1;
    }
}
