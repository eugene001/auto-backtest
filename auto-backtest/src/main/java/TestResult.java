import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by xuyujie on 2017/5/21.
 */
public class TestResult {

    private static List<List<Object>> result = new ArrayList<List<Object>>();
    private String range = "";
    private String date = "";

    public String getRange() {
        return range;
    }
    public static List<List<Object>> getResult(String data) {
        List<Object> fold = new ArrayList<Object>();

        String[] ctrs = data.split(",",3);
        fold.add(ctrs[0]);
        fold.add(ctrs[1]);
        fold.add(ctrs[2]);

        result.add(fold);
   //     result.add(fold2);
        return result;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setRange(String range) {
        this.range  = range;
    }


}
